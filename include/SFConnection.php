<?php

namespace SFProject;

use \ReflectionClass;
use \SforceEnterpriseClient;

class SFConnection
{
	/**
	 * @var SforceEnterpriseClient
	 */
	public $client;

	/**
	 * @param string $username SalesForce account username.
	 * @param string $password SalesForce account password.
	 * @param string $securityToken SalesForce account security token - may be
	 * 		  reset and retrieved on SalesForce website:
	 * 		  Settings > My Personal Information > Reset My Security Token
	 */
	public function __construct(
		string $username,
		string $password,
		string $securityToken)
	{
		$this->client = new SforceEnterpriseClient();

		// The wsdl file is alongside the SforceEnterpriceClient class
		$this->client->createConnection(
			dirname((new ReflectionClass($this->client))->getFilename())
				. "/enterprise.wsdl.xml");
		
		$this->client->login($username, $password . $securityToken);
	}
}