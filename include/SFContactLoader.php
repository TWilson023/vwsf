<?php

namespace SFProject;

class SFContactLoader
{
	/**
	 * @var SFConnection
	 */
	private $conn;

	/**
	 * @param SFConnection $conn
	 */
	public function __construct(SFConnection $conn)
	{
		$this->conn = $conn;
	}

	/**
	 * Loads all contacts from SalesForce.
	 * 
	 * @return array stdClass objects with Id and Name properties.
	 */
	public function loadAll(): array
	{
		$res = $this->conn->client->query("SELECT Id, Name from Contact");

		return $res->records;
	}
}