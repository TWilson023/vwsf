<?php

$rootDir = dirname(__DIR__);
require_once "$rootDir/vendor/autoload.php";

/**
 * Establishes SalesForce connection and loads contacts.
 *
 * @return array|string Array of contact objects (stdClass) or error string.
 */
function load($config) {
	try
	{
		$fromReq = isset($_POST['username'], $_POST['password'], $_POST['securityToken']);
		// Create/establish SalesForce connection
		$conn = new SFProject\SFConnection(
			$fromReq ? $_POST['username'] : $config['username'],
			$fromReq ? $_POST['password'] : $config['password'],
			$fromReq ? $_POST['securityToken'] : $config['securityToken']);
			
		try
		{
			// Load contacts with SFContactLoader
			return (new SFProject\SFContactLoader($conn))->loadAll();
		}
		catch (Exception $e)
		{
			http_response_code(500);
			return "Failed to load contacts:\n" . $e->getMessage();
		}
	}
	catch (Exception $e)
	{
		if ($e->faultcode === 'sf:INVALID_LOGIN' && isset($_POST['password']))
			http_response_code(401);
		else
			http_response_code(500);
		
		return "Failed to connect to SalesForce:\n" . $e->getMessage();
	}
	
	return [];
}

$config = require "$rootDir/config.php";
$result = load($config);

if (!is_array($result))
	$error = $result;
	
if (is_array($result))
{
	header("Content-Type: application/json");
	die(json_encode($result));
}

die($error);