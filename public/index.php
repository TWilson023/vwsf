<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>VW Salesforce Project</title>

		<link href="resources/css/main.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
	</head>
	<body>
		<main>
			<!-- Form -->
			<div id="result" class="hidden">
				<h1>Here's your SalesForce Contacts:</h1>
				<form>
					<select name="contact" id="contact-select"></select>
				</form>
			</div>
			<div class="error" id="error"></div>
			
			<!-- One-Time Configuration -->
			<button class="expand-button" id="expand-button" onclick="toggleConfig()">Show Options</button>
			<div class="configuration" id="configuration">
				<p>
					These options will only be used once.<br />
					We won't store your credentials.
				</p>
				<input type="text" id="username" placeholder="Username / Email" /><br />
				<input type="password" id="password" placeholder="Password" /><br />
				<input type="text" id="token" placeholder="Security Token" /><br />
				<button class="submit" type="button" onclick="update()">Load</button>
			</div>
		</main>
		
		<!-- Loading spinner -->
		<div class="loading" id="loading">
			<img class="spinner" src="resources/img/loading.gif" />
		</div>
		
		<script src="resources/js/main.js"></script>
	</body>
</html>