// Custom element utility functions
var Utils = {
	addClass: function (el, className) {
		if (el.classList)
			el.classList.add(className);
		else
			el.className += " " + className;
	},
	removeClass: function (el, className) {
		if (el.classList)
			el.classList.remove(className);
		else if (el.className)
			el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(" ").join('|') + '(\\b|$)', 'gi'), ' ');
	},
	hasClass: function (el, className) {
		if (el.classList)
			return el.classList.contains(className);
		else
			return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
	}	
};

function load(options) {
	options = options || {};
	
	// Build URLSearchParams to send as x-www-form-urlencoded
	var params = new URLSearchParams();
	for (var prop in options)
		if (options.hasOwnProperty(prop))
			params.append(prop, options[prop]);
			
	var elResult = document.getElementById('result'),
		elError = document.getElementById('error')
		elLoading = document.getElementById('loading');
		
	Utils.removeClass(elLoading, 'hidden');
	axios.post('load_contacts.php', params).then(function (result) {
		// Show result, hide error
		Utils.addClass(elError, 'hidden');
		Utils.removeClass(elResult, 'hidden');
		
		var select = document.getElementById('contact-select');
		select.innerHTML = "";
		
		// Create and add option elements
		result.data.map(function (item) {
			var option = document.createElement('option');
			option.value = item.Id;
			option.text = item.Name;
			select.add(option);
		});
	}).catch(function (error) {
		// Show error, hide result
		elError.innerText = error.response.data;
		Utils.removeClass(elError, 'hidden');
		Utils.addClass(elResult, 'hidden');
	}).then(function () {
		// Remove the loading overlay regardless of result
		Utils.addClass(elLoading, 'hidden');
	});
}

function toggleConfig() {
	var config = document.getElementById('configuration'),
		button = document.getElementById('expand-button');
	
	if (Utils.hasClass(config, 'expanded')) {
		Utils.removeClass(config, 'expanded');
		button.innerText = "Show Options";
	} else {
		Utils.addClass(config, 'expanded');
		button.innerText = "Hide Options";
	}
}

function update() {
	load({
		username: document.getElementById('username').value,
		password: document.getElementById('password').value,
		securityToken: document.getElementById('token').value
	});
}

load();