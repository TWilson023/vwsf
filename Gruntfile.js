module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.initConfig({
		stylus: {
			compile: {
				files: [{
					expand: true,
					cwd: "resources/styl",
					src: ["*.styl"],
					dest: "public/resources/css",
					ext: ".css"
				}]
			}
		},

		postcss: {
			options: {
				processors: [
					require('pixrem')(),
					require('autoprefixer')({ browsers: ['last 2 versions', 'ie 9'] }),
				]
			},
			dist: {
				src: 'public/resources/css/*.css'
			}
		},
		
		concat: {
			js: {
				src: ['resources/js/axios.min.js', 'resources/js/main.js'],
				dest: 'public/resources/js/main.js'
			}
		},
		
		uglify: {
			js: {
				files: {
					'public/resources/js/main.js': 'public/resources/js/main.js'
				}
			}
		},

		watch: {
			css: {
				files: ['resources/styl/*.styl'],
				tasks: ['stylus', 'postcss'],
				options: {
					spawn: false
				}
			},
			js: {
				files: ['resources/js/*.js'],
				tasks: ['concat', 'uglify'],
				options: {
					spawn: false
				}
			}
		}
	});

	grunt.registerTask('dist', ['stylus', 'postcss', 'concat', 'uglify']);
};